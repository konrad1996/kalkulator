using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace Kalkulator
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        OperacjeMatematyczne op = new OperacjeMatematyczne();
        double wartosc = 0; //Zmienna przechowująca wartość z wyświetlacza
        
        string operacja = "";/// <summary>
                             /// Deklarowanie operacji takich jak dzielenie mnożenie itp.
                             /// </summary>
        bool operacja_w = false;/// <summary>
                                /// Zmienna która będzie sprawdzała czy przycisk operacji został wciśnięty czy nie.
                                /// </summary>


        public MainWindow()
        {
            InitializeComponent();
        }

        private void p_Click(object sender, RoutedEventArgs e) /// Funkcja kliknięcia na przycisk
        {
            if ((txtwyswietlacz.Text == "0") || (operacja_w))///Czyszczenie wyświetlacza 
            {
                txtwyswietlacz.Clear();  //Czyszczenie wyświetlacza
                operacja_w = false; //Sprawdzanie czy operacja została wciśnięta
            }
            Button b = (Button)sender;

            if (b.Content.ToString() == ",")///Warunek dzięki któremu nie możemy wprowadzać więcej niż jednego ","
            {
                if (!txtwyswietlacz.Text.Contains(",")) //Sprawdzenie czy liczba zawiera przecinek
                    txtwyswietlacz.Text = txtwyswietlacz.Text + b.Content.ToString(); //Jeżeli nie zawiera zezwala na dodanie 
            }
            else
                txtwyswietlacz.Text = txtwyswietlacz.Text + b.Content.ToString();//Jeżeli przecinek został już wpisany zabrania dodania kolejnego

        }

        private void pC_Click(object sender, RoutedEventArgs e)///Przycisk Clear
        {
            txtwyswietlacz.Clear();//Czyszczenie wyświetlacza
            wartosc = 0;//Wyczyszczenie wartości
            pamiec.Content = string.Empty;
            txtwyswietlacz.Text = "0";
        }

        private void pznak_Click(object sender, RoutedEventArgs e)///Zmiana znaku liczby
        {
            if (!Double.TryParse(txtwyswietlacz.Text, out wartosc))
                wartosc = 0;
            wartosc = wartosc * (-1);//Mnożenie przez ujemną "1" aby zmienić znak liczby
            txtwyswietlacz.Text = wartosc.ToString();
        }

        private void Op_Click(object sender, RoutedEventArgs e)
        {
            Button b = (Button)sender;///Zczytanie wartości z wciśniętego klawisza
            operacja = b.Content.ToString();///Konwersja operacji na zmienną STRING
            if(!Double.TryParse(txtwyswietlacz.Text, out wartosc))
                wartosc = 0;

            operacja_w = true;
            pamiec.Content = wartosc + " " + operacja;///Zapisywanie do pamięci ostatniej liczby wraz z operacją
        }

        private void prowna_Click(object sender, RoutedEventArgs e)///Wykonywanie działań
        {
            pamiec.Content = "";
            switch (operacja)
            {
                case "+":
                    txtwyswietlacz.Text = op.Dodaj(wartosc, txtwyswietlacz.Text);//Odwołanie do metody dodawania oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "-":
                    txtwyswietlacz.Text = op.Odejmij(wartosc, txtwyswietlacz.Text);//Odwołanie do metody odejmowania oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "*":
                    txtwyswietlacz.Text = op.Mnozenie(wartosc, txtwyswietlacz.Text);//Odwołanie do metody mnożenia oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "/":
                    txtwyswietlacz.Text = op.Dzielenie(wartosc, txtwyswietlacz.Text);//Odwołanie do metody dzielenia oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "^":
                    txtwyswietlacz.Text = op.Potega(wartosc, txtwyswietlacz.Text);//Odwołanie do metody potęgowania oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "%":
                    txtwyswietlacz.Text = op.Procent(wartosc, txtwyswietlacz.Text);//Odwołanie do metody procentowania oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "√":
                    txtwyswietlacz.Text = op.Pierwiastek(wartosc);//Odwołanie do metody pierwiastkowania oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "mod":
                    txtwyswietlacz.Text = op.Modulo(wartosc, txtwyswietlacz.Text);//Odwołanie do metody modulo oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "bin":
                    txtwyswietlacz.Text = op.Binarnie(txtwyswietlacz.Text);//Odwołanie do metody zamiany na liczbę binarną oraz wykonanie operacji po wciśnięciu "="
                    break;
                case "!":
                    txtwyswietlacz.Text = op.Silnia(txtwyswietlacz.Text);//Odwołanie do metody silnia oraz wykonanie operacji po wciśnięciu "="
                    break;
                default:
                    break;

            }//koniec switcha
            operacja_w = false;

        }
    }

    public class OperacjeMatematyczne
    {
        public string Dodaj(double a, string b)//Dodawanie liczb
        {
            double c;//Przypisanie wartości 
            if(!Double.TryParse(b, out c))
                return a.ToString();//zmiana typu zmiennej na string
            return (a + c).ToString();//zwraca oraz dodaje liczby
        }

        public string Odejmij(double a, string b)//Odejmowanie liczb 
        {
            double c;
            if (!Double.TryParse(b, out c))
                return a.ToString();
            return (a - c).ToString();
        }

        public string Mnozenie(double a, string b)//Mnożenie liczb
        {
            double c;
            if (!Double.TryParse(b, out c))
                return a.ToString();
            return (a * c).ToString();
        }

        public string Dzielenie(double a, string b)//Dzielenie liczb
        {
            double c;
            if (!Double.TryParse(b, out c))
                return a.ToString();
            return (a/c).ToString();
        }

        public string Potega(double a, string b)//Potęgowanie liczb
        {
            double c;
            if (!Double.TryParse(b, out c))
                return a.ToString();
            return Math.Pow(a, c).ToString();
        }

        public string Procent(double a, string b)//Procent z liczby
        {
            double c;
            if (!Double.TryParse(b, out c))
                return a.ToString();
            return (a / c).ToString("0.00%");
        }

        public string Pierwiastek(double a)//Pierwiastek z liczby
        {
            return Math.Sqrt(a).ToString();
        }

        public string Modulo(double a, string b)//Modulo 
        {
            double c;
            if (!Double.TryParse(b, out c))
                return a.ToString();
            return (a % c).ToString();
        }

        public string Binarnie(string a)//Zamiana na binarne użycie wbudowanego konwertera w C#
        {
            int b;
            if (!int.TryParse(a, out b))
                b = 0;

            return Convert.ToString(b, 2);
        }

        public string Silnia(string a)//SIlnia
        {
            int b = 0;
            if (!int.TryParse(a, out b))
                return "1";

            int wynik = 1;

            for(int i = 1; i <= b; i++)
            {
                wynik *= i;
            }

            return wynik.ToString();
        }
    }
}